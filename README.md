# Minimal bitbucket pipeline for yarn builds

This repository contains a `bitbucket-pipelines.yml` file that does the bare minimum of work needed to get a yarn build running on a remote host.

## Prerequisites
- Pipelines must be enabled for your repository;
- The public key for the pipeline must be added to `~/.ssh/authorized_keys` on the remote host;
- The `DEPLOY_USER` and `DEPLOY_HOST` repository variables must be set;
- The remote host must have rsync and unzip installed.

## Usage
Add this file in the root of your repository, then just commit & push to trigger the pipeline.